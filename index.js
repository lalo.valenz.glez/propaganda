var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var bluebird = require('bluebird');
var JWT    = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var config = require('./config/config'); 
var User   = require('./server/models/user');
var Evidence = require('./server/models/evidence'); 
var apiController = require('./server/controllers/api/index');
// =======================
// configuration =========
// =======================
var port = process.env.PORT || 8080;
mongoose.Promise = bluebird;
mongoose.connect(config.database)
    .then(function() {console.log('database is connected')})
    .catch (function(err) {console.log(err)});
app.set('superSecret', config.secret);
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
  });
// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser({limit: '50mb', extended: true, parameterLimit:50000}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '50mb'}));


// use morgan to log requests to the console
app.use(morgan('dev'));

// =======================
// routes ================
// =======================
// basic route
app.use('/api', apiController);

app.get('/', function(req, res) {
    res.send('Hello! The API is at http://localhost:' + port + '/api');
});
// app.get('/setup', function (req, res){
//     var admin = new User({
//         name: 'admin',
//         lastname: 'admin',
//         password: bcrypt.hashSync('password', 10),
//         username: 'admin@admin',
//         email: 'lalo.valenz.glez@gmail.com',
//         admin: true
//     });
//
//     admin.save(function(err, auser){
//         if(err) res.json({success: false, error: err.message});
//         console.log('user saved successfully');
//         var user = new User({
//             name: 'john',
//             lastname: 'smith',
//             password: bcrypt.hashSync('123456', 10),
//             username: 'johnsmith',
//             email: 'johnsmit@foo.com',
//             admin: false
//         });
//
//         user.save(function(err, user){
//             if(err) res.json({success: false, error: err.message});
//             console.log('user saved successfully');
//             res.json({success:true, user: user, admin: auser});
//         })
//     })
//
// });

// API ROUTES -------------------
// we'll get to these in a second

// =======================
// start the server ======
// =======================
app.listen(port);
console.log('Magic happens at http://localhost:' + port);