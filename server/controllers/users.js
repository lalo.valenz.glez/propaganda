var userService = require('../service/user');

exports.getUsers = async function(req, res, next) {
    try{
        var users = await userService.findAll({}, 1, 10);
        res.status(200).json({success: true, users: users});
    }catch(e){
        res.status(400).json({success: false, error: e.message});
    }
};

exports.createUser = async function(req, res, next) {

};
