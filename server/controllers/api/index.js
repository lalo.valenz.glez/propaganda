var express = require('express');
var router = express.Router();
var usersController = require('../users');
var JWT = require('jsonwebtoken');
var serviceToken = require('../../service/token');
var userService = require('../../service/user');
var evidenceService = require('../../service/evidence');
var middleware = require('../../middlewares/middleware');
var evidencesController = require('../evidences');
/**
 * Route app.io/api/autenthicate
 */
router.post('/authenticate', async function (req, res, next) {
    try{
        var user = await userService.findOne({username: req.body.user.username});
        if (!user) res.status(404).json({success: false, message: 'No se encuentra el usuario'});
        if (user) {
            if (!user.comparePassword(req.body.user.password))
                res.status(400).json({success: false, message: 'Contraseña incorrecta'});
            else {
                var token = serviceToken.createToken(user);
                var count = await evidenceService.countEvidenceByUser(user._id);
                res.status(200).json({success: true, user: user, token: token, count: count});
            }
        }
    }catch(e){
        console.log(e);
        if (err) res.status(500).json({success: false, error: err.message});
    }
});

/**
 * Route /api/users
 */
router.get('/users', middleware.ensureAuthenticated, usersController.getUsers);

/**
 * Rutas de evidencias
 */
router.get('/evidences', middleware.ensureAuthenticated, evidencesController.getEvidences);
router.post('/evidences',middleware.ensureAuthenticated, evidencesController.createEvidence);

module.exports = router;