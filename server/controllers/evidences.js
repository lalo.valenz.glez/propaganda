var evidenceService = require('../service/evidence');

exports.getEvidences = async function (req, res, next) {
    try {
        var user = req.user;
        if (user.admin) {
            var evidences = await evidenceService.getEvidences({}, req.params.page, req.params.limit)
            res.status(200).json({ success: true, evidences: evidences });
        } else {
            var evidences = await evidenceService.getEvidenceByUserEvidences(user._id);
            res.status(200).json({ success: true, evidences: evidences });
        }

    } catch (e) {
        res.status(400).json({ success: false, error: e.message });
    }
}

exports.getEvidenceById = async function (req, res, next) {
    try {
        
    } catch (e) {
        res.status(400).json({ success: false, error: e.message });
    }
}
exports.createEvidence = async function (req, res, next) {
    try {
        var evidence = await evidenceService.createEvidence(req.body.evidence, req.user.id);
        res.status(201).json({ success: true, evidence: evidence });
    } catch (e) {
        res.status(400).json({ success: false, error: e.message });
    }
}

exports.updateEvidence = async function (req, res, next) {
    try {

    } catch (e) {
        res.status(400).json({ success: false, error: e.message });
    }
}

exports.deleteEvidence = async function (req, res, next) {
    try {

    } catch (e) {
        res.status(400).json({ success: false, error: e.message });
    }
}

