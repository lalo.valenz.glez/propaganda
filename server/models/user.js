var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');
var bcrypt = require('bcrypt');
var UserSchema =new Schema({
    username: String,
    name: String,
    lastname: String,
    password: String,
    email: String,
    admin: Boolean
});

UserSchema.plugin(mongoosePaginate);
UserSchema.methods.comparePassword = function(password){
    return bcrypt.compareSync(password, this.password);
}

module.exports =  mongoose.model('User', UserSchema);