var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');

var EvidenceSchema =  new Schema({
    user_id: String,
    image_path: String,
    lat: String,
    lng: String,
    observations: String,
    create_at: Date
});
EvidenceSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Evidence', EvidenceSchema);