var jwt = require('jsonwebtoken');
var moment = require('moment');
var config = require('../../config/config');

exports.createToken = function (user) {
    var payload = {
        id: user._id,
        iat: moment().unix(),
        expires_in: moment().add(1, 'hour').unix(),
        admin: user.admin
    };
    return jwt.sign(payload, config.secret, {expiresIn: '1h'});
};
