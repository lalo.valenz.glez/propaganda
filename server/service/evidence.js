var Evidence = require('../models/evidence');

exports.getEvidences = async function(query, page, limit){
    try{
        var evidences = await Evidence.find(query);
        return evidences;
    }catch(e){
        console.log(e)
        throw Error('Error while fetch all evidences');
    }
}

exports.createEvidence = async function(evidence, user_id){
    try {
        var newEvidence = new Evidence({
            user_id: user_id,
            image_path: evidence.image_path,
            lat: evidence.lat,
            lng: evidence.lng,
            observations: evidence.observations,
            create_at: new Date()
        });
        var e = await newEvidence.save();
        return e;
    }catch(e){
        console.log(e);
        throw Error('Error while saving evidence');
    }
}

exports.countEvidenceByUser = async function(user_id){
    try{
        var count = await Evidence.find({user_id: user_id}).count();
        return count;
    }catch(e){
        throw Error('ups something went wrong while counting the evidences')
    }
}

exports.getEvidenceByUser = async function(user_id){
    try{
        var evidences = await Evidence.find({user_id: user_id});
        return evidences;
    }catch(e){
        throw Error('ups something went wrong while get all evidences');
    }
}