var User = require('../models/user');
var bcrypt = require('bcrypt');

exports.findAll = async function(query, page, limit) {
    try{
        var users = await User.paginate(query,{
            page: page,
            limit: limit
        });
        return users;
    }catch (e){
        throw Error('Error While Paginating Users');
    }
};

exports.findOne = async function(query){
    try{
        var user = await User.findOne(query);
        return user;
    }catch(e){
        throw Error('Error While Finding User');
    }
}

exports.findOneById = async function(id){
    try{
        var user = await User.findById(id);
        return user;
    }catch(e){
        throw Error('Error Whilep Finding User');
    }
}

exports.createUser = async function(user){
    var newUser = new User(user);
    newUser.password = bcrypt.hashSync(user.password, 10);
    try{
        var u = await newUser.save();
        u.password = undefined;
        return u;
    }catch(e){
        throw Error(e.message);
    }
}