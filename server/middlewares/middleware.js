var jwt = require('jsonwebtoken');
var moment = require('moment');
var config = require('../../config/config');
var User = require('../models/user');

exports.ensureAuthenticated = function(req, res, next){
    if(!req.headers.authorization)
        res.status(403).json({success: false, message:"tu peticion no tiene cabezera de autenticacion"});
    var token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, config.secret, function(err, decoded){
        if(err) res.status(400).json({success: false, error: err.message});
        var payload = decoded;
        if(payload.expires_in <= moment().unix())
            res.status(401).json({success: false, message:"el token ha expirado"});
        req.user = {id: payload.id, admin: payload.admin};
        next();
    });
};